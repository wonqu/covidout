import axios from 'axios';

import {
    SYMPTOM_BIEGUNKA,
    SYMPTOM_BOL_BRZUCHA,
    SYMPTOM_BOL_GARDLA,
    SYMPTOM_BOL_GLOWY,
    SYMPTOM_BOL_MIESNI,
    SYMPTOM_DUSZNOSC,
    SYMPTOM_GORACZKA,
    SYMPTOM_KASZEL,
    SYMPTOM_KATAR,
    SYMPTOM_KICHANIE,
    SYMPTOM_KOLATANIE_SERCA,
    SYMPTOM_KRWIOPLUCIE,
    SYMPTOM_NIEPOKOJ,
    SYMPTOM_NUDNOSCI,
    SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH,
    SYMPTOM_SPLATANIE,
    SYMPTOM_TEMPERATURA,
    SYMPTOM_WYSYPKA,
    SYMPTOM_ZABURZENIE_SMAKU,
    SYMPTOM_ZABURZENIE_WECHU,
    SYMPTOM_ZAPARCIA,
    SYMPTOM_ZAWROTY_GLOWY,
    SYMPTOM_ZGAGA,
    SYMPTOM_ZIME_DRESZCZE,
    SYMPTOM_ZMECZENIE
} from "./consts";

const MAKE_REQUEST = (url, setState, post_data: object) => {
    axios.post(url, post_data).then(
        res => {
            const data = res.data;
            setState(data);
        }
    )
};

const DOMAIN =                    `${window.location.origin}`;
const ROUTE_GET_DAY_NOTE =        `${DOMAIN}/calendar`;
const ROUTE_GET_NOTIFICTIONS =    `${DOMAIN}/notfictions`;
const ROUTE_GET_CONTACT_NUMBERS = `${DOMAIN}/contact`;
const ROUTE_UPLOAD_SUMPTOMS =     `${DOMAIN}/sumptoms`;
const ROUTE_UPLOAD_GPS =          `${DOMAIN}/location`;
const ROUTE_IS_DAY_EMPTY =        `${DOMAIN}/emptyday`;

interface GET_DAY_NOTE_INTERFACE {
    user_id: number
    date: string
}
export const POST_GET_DAY_NOTE = (params: GET_DAY_NOTE_INTERFACE, setState) => MAKE_REQUEST(
    ROUTE_GET_DAY_NOTE,
    setState,
    params,
);

interface GET_NOTIFICTIONS_INTERFACE {
    user_id: number
    date: string
}
export const POST_GET_NOTIFICTIONS = (params: GET_NOTIFICTIONS_INTERFACE, setState) => MAKE_REQUEST(
    ROUTE_GET_NOTIFICTIONS,
    setState,
    params
);

interface GET_CONTACT_NUMBERS_INTERFACE {
    user_id: number
}
export const POST_GET_CONTACT_NUMBERS = (params: GET_CONTACT_NUMBERS_INTERFACE, setState) => MAKE_REQUEST(
    ROUTE_GET_CONTACT_NUMBERS,
    setState,
    params
);

interface SYMPTOMS_INTERFACE {
    user_id: number,
    date: string,
    symptoms: {
        [SYMPTOM_KATAR]?: boolean,
        [SYMPTOM_KICHANIE]?: boolean,
        [SYMPTOM_NUDNOSCI]?: boolean,
        [SYMPTOM_BIEGUNKA]?: boolean,
        [SYMPTOM_BOL_BRZUCHA]?: boolean,
        [SYMPTOM_ZAWROTY_GLOWY]?: boolean,
        [SYMPTOM_NIEPOKOJ]?: boolean,
        [SYMPTOM_KOLATANIE_SERCA]?: boolean,
        [SYMPTOM_ZIME_DRESZCZE]?: boolean,
        [SYMPTOM_KRWIOPLUCIE]?: boolean,
        [SYMPTOM_SPLATANIE]?: boolean,
        [SYMPTOM_WYSYPKA]?: boolean,
        [SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH]?: boolean,
        [SYMPTOM_ZAPARCIA]?: boolean,
        [SYMPTOM_ZGAGA]?: boolean,
        [SYMPTOM_ZMECZENIE]?: boolean,
        [SYMPTOM_BOL_GLOWY]?: boolean,
        [SYMPTOM_BOL_MIESNI]?: boolean,
        [SYMPTOM_BOL_GARDLA]?: boolean,
        [SYMPTOM_ZABURZENIE_WECHU]?: boolean,
        [SYMPTOM_ZABURZENIE_SMAKU]?: boolean,
        [SYMPTOM_KASZEL]?: boolean,
        [SYMPTOM_GORACZKA]?: boolean,
        [SYMPTOM_DUSZNOSC]?: boolean,
        [SYMPTOM_TEMPERATURA]?: boolean,
    }
}
export const POST_UPLOAD_SUMPTOMS = (params: SYMPTOMS_INTERFACE, setState) => MAKE_REQUEST(
    ROUTE_UPLOAD_SUMPTOMS,
    setState,
    params
);

interface UPLOAD_GPS_INTERFACE {
    user_id: number
    gps: {
        datatime: string
        long: number
        lang: number
    }
}
export const POST_UPLOAD_GPS = (params: UPLOAD_GPS_INTERFACE, setState) => MAKE_REQUEST(
    ROUTE_UPLOAD_GPS,
    setState,
    params
);
