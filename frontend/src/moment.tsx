import moment from "moment-timezone";

moment.tz.setDefault(moment.tz.guess());
export const MOMENT = moment;