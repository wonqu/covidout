export const ARTICLES = [{
        'title': 'Czym jest koronawirus? ',
        'content': 'Jest to wirus RNA osłonięty błoną tłuszczową (lipidową). Dzięki takiej budowie można mu zapobiegać przez zastosowanie środków chemicznych, takich jak zwykłe mydło, alkohol min. 60-70%, preparaty do dezynfekcji i inne wirusobójcze.'
    }, {
        'title': 'Jaką chorobę wywołuje?',
        'content': 'Nowy koronawirus SARS-Cov-2 wywołuje chorobę o nazwie COVID-19. Najczęściej występujące objawy choroby to gorączka, kaszel, duszność, problemy z oddychaniem. Chorobie mogą towarzyszyć bóle mięśni i zmęczenie.'
    }, {
        'title': 'Jak często występują objawy?',
        'content': 'W 80% przypadków choroba przebiega łagodnie. Ciężki przebieg choroby obserwuje się u ok. 15–20% osób. Do zgonów dochodzi u 2–3% osób chorych. Światowa Organizacja Zdrowia (WHO) szacuje, że śmiertelność poza Chinami wynosi tylko 0,7%.\nNajbardziej narażone na rozwinięcie ciężkiej postaci choroby i zgon, są osoby starsze, z obniżoną odpornością, którym towarzyszą inne choroby, w szczególności przewlekłe. Należy jednak pamiętać o tym, że nie można wykluczyć możliwości ciężkiego przebiegu choroby u osób młodych.'
    }, {
        'title': 'Jak mogę się zakazić?',
        'content': 'Wirus przenosi się drogą kropelkową. Do zakażenia dochodzi kiedy zakażona osoba kaszle lub kicha, podobnie jak przenosi się np. wirus grypy. Wirus może także zostać przeniesiony, kiedy człowiek dotknie powierzchni lub przedmiotu, na którym znajduje się wirus (czyli np. ślina osoby chorej), a następnie dotknie swoich ust, nosa lub oczu.\nAktualnie nie ma szczepionki przeciw nowemu koronawirusowi. Można natomiast stosować inne metody zapobiegania zakażeniu, zaprezentowane poniżej. Metody te stosuje się również w przypadku zapobiegania innym chorobom przenoszonym drogą kropelkową np. grypie sezonowej.'
    }, {
        'title': 'Czy mogę się zakazić od osoby wyleczonej?',
        'content': 'Obecnie brak dowodów naukowych na transmisję wirusa od ozdrowieńców.'
    }, {
        'title': 'Kiedy należy poddać się kwarantannie? ',
        'content': '14-dniowej kwarantannie powinny się poddać osoby, które miały bliski kontakt z chorym na koronawirusa lub wróciły z krajów transmisji wspomnianego wirusa.\nWedług Światowej Organizacji Zdrowia (WHO) okres inkubacji, czyli okres od zakażenia pacjenta a początkiem występowania u niego objawów, w przypadku wirusa powodującego COVID-19 waha się pomiędzy 1-14 dni, najczęściej jest to około 5 dni (WHO zastrzega, że informacje te mogą być akutalizowane, w miarę spływania danych). Natomiast u części zakażonych objawy mogą w ogóle nie występować. Niestety osoby bez objawów mogą być źródłem zakażenia.'
    }, {
        'title': 'Co to znaczy, że ktoś miał kontakt z osobą zakażoną koronawirusem?',
        'content': 'pozostawał w bezpośrednim kontakcie z osobą chorą lub w kontakcie w odległości mniej niż 2 metrów przez ponad 15 minut\nprowadził rozmowę z osobą z objawami choroby twarzą w twarz przez dłuższy czas\nosoba zakażona należy do grupy najbliższych przyjaciół lub kolegów\nosoba mieszkająca w tym samym gospodarstwie domowym, co osoba chora, lub w tym samym pokoju hotelowym'
    }, {
        'title': 'Jak mogę chronić się przed zakażeniem? ',
        'content': '1. Myj często ręce\nMycie rąk wodą z mydłem lub środkiem na bazie alkoholu zabija wirusy, jeśli są na twoich rękach.\n2. Zachowuj zasady higieny układu oddechowego\nJeśli kichasz lub kaszlesz, zasłoń usta i nos zgiętym ramieniem (kichaj w zgięcie łokcia) lub chusteczką, którą następnie wyrzuć natychmiast do zamykanego kosza na śmieci i umyj starannie ręce – patrz punkt 1.\nZasłanianie ust i nosa podczas kaszlu zapobiega rozprzestrzenianiu się bakterii i wirusów. Jeśli podczas kaszlu lub kichania zasłaniasz się dłonią, możesz następnie przenieść drobnoustroje na innych ludzi (np. podając im rękę) lub przedmioty (np. dotykając klamek, włączników światła i innych powierzchni).\n3. Utrzymuj odpowiednią odległość\nNie zbliżaj się do innych na mniej niż 1 metr, a zwłaszcza do osób, które kaszlą, kichają lub mają gorączkę.\nKiedy osoba mająca zakażenie układu oddechowego, taką jak koronawirus, kaszle lub kicha, może rozprzestrzeniać małe ilości wydzieliny („kropelki”, stąd nazwa przenoszenia takich chorób – droga kropelkowa) zawierającej wirus. Jeśli będziesz blisko, możesz zostać zakażony.\n4. Unikaj dotykania oczu, nosa i ust\nTwoje dłonie mają kontakt z różnymi powierzchniami, na których może znajdować się wirus. Jeśli dotykasz później oczu, nosa lub ust, możesz przenieść na siebie zakażenie.'
    }, {
        'title': 'Jak prawidłowo zmierzyć temperaturę ciała?',
        'content': 'Aby prawidłowo kontrolować temperaturę ciała należy wziąć pod uwagę to, że może ona różnić się w zależności od miejsca pomiaru. Dlatego podstawową zasadą jest pomiar wykonywany w tym samym miejscu ciała, za pomocą tego samego sprzętu.\nWarunkiem bezwzględnym wiarygodnego pomiaru temperatury pod pachą (prawidłowa wartość 36,6°C) jest silne ściśnięcie termometru pod pachą i odpowiednio długi czas pomiaru – w termometrach elektronicznych pomocna bywa sygnalizacja dźwiękowa.\nDokładniejszy jest pomiar w jamie ustnej (zwykle o trzy kreski wyższy) albo pomiar ciepłoty rektalnej (mierzonej w odbytnicy, zwłaszcza u małych dzieci). Prawidłowo wynosi on o pięć kresek więcej niż pod pachą. Identyczny wynik (37,1°C) otrzymuje się mierząc temperaturę w uchu (jest to temperatura błony bębenkowej). Ta ocena zajmuje mało czasu i dlatego jest chętnie stosowana w szpitalu, trzeba jednak pamiętać, że pomiar ten bywa niedokładny z powodu obecności woskowiny. W przypadku termometrów bezdotykowych warto dokładnie zapoznać się z dołączoną instrukcją obsługi urządzenia.\nPrzyjmujemy, że za gorączkę uważa się ciepłotę ciała powyżej 38,0°C. Zakres 37,1–38,0°C to tak zwany stan podgorączkowy'
    }, {
        'title': 'Czy maseczki chirurgiczne chronią przed zakażeniem koronawirusem?',
        'content': 'Noszenie maseczki zasłaniającej usta i nos może pomóc ograniczyć rozprzestrzenianie się niektórych chorób układu oddechowego.  Jednak stosowanie samej maseczki nie gwarantuje powstrzymania infekcji i powinno być połączone ze stosowaniem innych środków zapobiegawczych, w tym higieną rąk i zasadami ochrony podczas kaszlu czy kichania oraz unikaniem bliskiego kontaktu z innymi ludźmi (co najmniej 1 metr odległości).\nUżywaj  maseczek tylko wtedy, gdy masz objawy ze strony układu oddechowego (kaszel lub kichanie), podejrzewasz u siebie infekcję SARS-Cov-2 przebiegającą z łagodnymi objawami lub opiekujesz się osobą z podejrzeniem infekcji SARS-Cov-2.'
    }, {
        'title': 'Czy są jakieś specjalne leki zapobiegające nowemu koronawirusowi lub leczące go?',
        'content': 'Do tej pory nie ma konkretnego leku zalecanego do zapobiegania lub leczenia nowego koronawirusa. Osoby zakażone wirusem otrzymują leczenie objawowe oraz leczenie ewentualnych powikłań bakteryjnych.\nDo obrotu są dopuszczone leki, które stosowane w leczeniu wspomagającym w zakażeniu koronawirusem u chorych z ciężkim przebiegiem choroby. Nie stosuje się ich profilaktycznie ani u osób z innym niż ciężki przebiegiem choroby.'
    }, {
        'title': 'Czy antybiotyki są skuteczne w zapobieganiu nowemu koronawirusowi i w jego leczeniu?',
        'content': 'Nie, antybiotyki działają przeciwko bakteriom, ale nie działają przeciwko wirusom. Nowy koronawirus jest wirusem, dlatego antybiotyki nie powinny być stosowane jako środek zapobiegawczy lub leczniczy. \nOsoby hospitalizowane z powodu koronawirusa mogą otrzymywać antybiotyki, jeśli wystąpi zakażenie bakteryjne, wtórne w stosunku do wirusowego.'
    }, {
        'title': 'Czy zakażona kobieta w ciąży może przekazać wirusa nienarodzonemu dziecku lub podczas porodu?',
        'content': 'Obecnie brak dowodów naukowych wskazujących na transmisję wirusa SARS-CoV-2 od matki do płodu. Dostępne badania naukowe wśród noworodków urodzonych przez matki z COVID-19 wskazują, że żadne z noworodków nie miało wyniku pozytywnego na obecność wirusa powodującego COVID-19. Dodatkowo nie stwierdzono wirusa w próbkach płynu owodniowego ani w mleku matki. \nNiemniej jednak, należy pamiętać, że każde zagrożenie dla zdrowia i życia ciężarnej kobiety jest również zagrożeniem dla zdrowia i życia jej nienarodzonego dziecka. Nawet jeśli wirus nie zostanie przeniesiony na płód lub noworodka, pogorszenie stanu zdrowia matki, spowodowane przez chorobę, może zagrozić życiu i zdrowiu jej nienarodzonego dziecka. Dlatego, w przypadku wystąpienia niepokojących objawów, zwłaszcza w przypadku kobiety ciężarnej, zalecane jest poszukanie pomocy medycznej.'
    }, {
        'title': 'Dlaczego należy suplementować witaminę D?',
        'content': 'Dieta pokrywa maksymalnie 20% dziennego zapotrzebowania na witaminę D. Prawie 90% witaminy D w organizmie jest syntetyzowane w skórze (witamina D3 – cholekalcyferol) pod wpływem promieniowania słonecznego UVB. Niektóre choroby przewlekłe mogą powodować niedobór witaminy D w organizmie. Obecnie natomiast, z uwagi na obowiązek pozostawania w domu, zarówno u zdrowych jak i chorych osób, istnieje wysokie ryzyko obniżenia poziomu tej witaminy w organizmie. '
    }, {
        'title': 'Czym grozi niedobór witaminy D w organizmie? ',
        'content': 'Związany jest ze spadkiem odpornośći immunologicznej organizmu, zwiększonym ryzykiem rozwoju niektórych chorób, takich jak choroby nowotworowe (np. okrężnicy, piersi, jajników, prostaty), autoimmunologiczne (np. cukrzyca typu 1, choroba Hashimoto) czy cywilizacyjne (np. miażdżyca, otyłość, cukrzyca typu 2, choroby sercowo-naczyniowe). Obserwuje się także zależność między deficytem witaminy D w organizmie a zwiększonym ryzykiem chorób neurodegeneracyjnych (np. choroba Alzheimera) i zaburzeń psychiatrycznych (np. schizofrenia, depresja).\nDawki rekomendowane w suplementacji witaminy D w populacji ogólnej (chyba że lekarz zalecił inaczej):\nu kobiet w ciąży i karmiących piersią: 2000 IU/dobę\nu noworodków donoszonych i niemowląt: 400-600 IU/dobę\nu noworodków niedonoszonych: skonsultuj się z lekarzem\nu dzieci w wieku 1–10 lat: 600-1000 IU/dobę; u dzieci z otyłością: 1600-4000 IU/dobę w zależności od stopnia otyłości\nu młodzieży w wieku 11–18 lat: 800-2000 IU/dobę; u młodzieży z otyłością: 1600-4000 IU/dobę w zależności od stopnia otyłości\nu dorosłych i seniorów 65-75 r.ż.: 800-2000 IU/dobę; u dorosłych z otyłością: 1600-4000 IU/dobę w zależności od stopnia otyłości\nu seniorów >75 r.ż.: 2000-4000 IU/dobę; u seniorów z otyłością 4000-8000 IU/dobę w zależności od stopnia otyłości\nSuplementację należy prowadzić do chwili, kiedy ustąpi obowiązek pozostawania w domu albo w innym, zaleconym przez lekarza, terminie. '
    }, {
        'title': 'Dlaczego w szkołach, przedszkolach, żłobkach i na uczelniach zawieszane są zajęcia?',
        'content': 'Działamy zapobiegająco i profilaktycznie. Dzięki takim działaniom ograniczymy w Polsce duże skupiska ludzi. Im mniej ludzi w jednym miejscu, tym mniejsze szanse na szybkie rozprzestrzenianie się koronawirusa lub innych wirusów. Stąd decyzja o ograniczeniu miejsc pełnych ludzi, żebyśmy mogli działać zapobiegawczo.\nDzięki tzw. specustawie (specjalnej ustawie dotyczącej szczególnych rozwiązań w walce z epidemią koronawirusa) rodzicom dzieci w wieku do lat 8, będzie przysługiwał dodatkowy zasiłek opiekuńczy w przypadku nieprzewidzianego zamknięcia żłobka, przedszkola, szkoły lub klubu dziecięcego.'
    }, {
        'title': 'Komu przysługuje dodatkowy zasiłek opiekuńczy na czas opieki nad dzieckiem?',
        'content': 'Dodatkowy zasiłek opiekuńczy przysługuje rodzicom lub opiekunom prawnym dzieci, którzy:\nopiekują się dzieckiem poniżej 8. roku życia\nsą objęci ubezpieczeniem chorobowym\nJak uzyskać dodatkowy zasiłek opiekuńczy?\nWystarczy złożyć oświadczenie do swojego pracodawcy. Wzór oświadczenia można pobrać na stronie internetowej ZUS www.zus.pl \nOsoby prowadzące działalność pozarolniczą składają oświadczenie w ZUS.'
    }, {
        'title': 'Co mogę robić z dzieckiem w domu? ',
        'content': 'grać w planszówki, karty, warcaby, szachy\nczytać książki, bajki, komiksy\noglądać pasma dla dzieci w telewizji lub filmy dokumentalne na platformach vod\nuczyć się – pamiętaj, że wiele szkół i przedszkoli decyduje się na naukę zdalną i mogą zlecać Wam i Waszym dzieciom różne zadania\nmoże czas na nowe hobby? Majsterkowanie, robienie na drutach czy gra na instrumencie może przypadnie najmłodszym do gustu\nleżakować (opcja również dla dorosłych)'
    }, {
        'title': 'Czy powinnam zgłosić się ze swoim dzieckiem na planowane szczepienie? ',
        'content': 'W związku z zakażeniami wirusem SARS-CoV-2 Minister Zdrowia oraz Główny Inspektor Sanitarny rekomendują odroczenie szczepień obowiązkowych w ramach Programu Szczepień Ochronnych u dzieci, na 30 dni od wydania komunikatu, tj. do 18 kwietnia 2020 r.\nSzczepienie może być wykonane w sytuacji zaistnienia uzasadnionych przesłanek medycznych – lekarz opiekujący się dzieckiem powinien podjąć wówczas indywidualną decyzję.\nSzczepienia w oddziałach noworodkowych oraz szczepienia poekspozycyjne (wścieklizna, tężec) powinny być prowadzone na dotychczasowych zasadach.'
    }, {
        'title': 'Czy wirus może przenosić się za pomocą żywności?',
        'content': 'Europejski Urząd ds. Bezpieczeństwa Żywności (EFSA) opublikował w dniu 9.03.2020 stanowisko, iż zgodnie z aktualną wiedzą nie ma dowodów na to, że żywność może być źródłem lub pośrednim ogniwem transmisji wirusa SARS COV-2.'
    }, {
        'title': 'Czy obróbka termiczna niszczy koronawirusa?',
        'content': 'Dokładna obróbka termiczna niszczy wirusa, ponieważ koronawirus ulega zniszczeniu gdy zastosuje się odpowiednią kombinację czasu i temperatury np. 60stC przez 30 min. Tak więc w przypadku mięsa, produktów surowych, typowa obróbka cieplna eliminuje zanieczyszczenie mikrobiologiczne, w tym również SARS COV-2.'
    }, {
        'title': 'Jakie działania powinien podjąć świadomy klient-konsument?',
        'content': 'W sklepie: \nosoby, które przebywają w sklepach, supermarketach, centrach handlowych powinny zawsze pamiętać o tym, że są współodpowiedzialne za zdrowie swoje i innych\nw miarę możliwości należy unikaj przebywania w miejscach zatłoczonych, utrzymywać odstępy w kolejkach\npamiętaj o higienie rąk oraz higienie kichania i kasłania, w szczególności przy pakowaniu żywności luzem, nieopakowanej, tj. pieczywo, produkty cukiernicze, orzechy\nręce w sklepie mają kontakt z powierzchniami roboczymi, pieniędzmi\ndo pakowania  używaj dostępnych rękawic foliowych oraz torebek\nniehigienicznym zachowaniem jest przebieranie i dotykanie produktów, które są przeznaczone do spożycia bez mycia i obróbki termicznej (np. pieczywo, bułki, wyroby cukiernicze)\nzwracaj uwagę na właściwą higienę układu oddechowego: nie kaszlaj lub/i nie kichaj w stronę innych osób czy towarów.\nW domu: https://gis.gov.pl/wp-content/uploads/2018/04/5-krok%C3%B3w-do-bezpieczniejszej-%C5%BCywno%C5%9Bci.pdf'
    }, {
        'title': 'Czy można zaKazić się koronawirusem 2019-nCoV poprzez towary zamawiane z Chin?',
        'content': 'Nie ma dowodów na to, żeby koronawirus 2019-nCoV mógłby rozprzestrzeniać się w ten sposób. Nawet jeśli koronawirus 2019-nCoV mógłby zostać przeniesiony na jakiś przedmiot (np. poprzez kichnięcie osoby zakażonej), pozostaje kwestia czasu dostarczenia przesyłki. Większość wirusów przeziębienia jest w stanie przetrwać poza organizmem człowieka mniej niż 24h, choć istnieją wirusy, które mogą przetrwać w ten sposób kilka miesięcy. \nNa razie wszystko wskazuje na to, że do przeniesienia zakażenia potrzebny jest bezpośredni kontakt z osobą chorą.\nIstnieją doniesienia, że wirus może przetrwać w aerozolu wytwarzanego przez osobę chorą (podczas kaszlu, kichania) w pomieszczeniu nawet do 3 godzin. Tak zaraziła się jedna osoba, która jechała autobusem, w którym wcześniej przebywał chory.'
    }, {
        'title': 'Czy zwierzęta domowe mogą przenosić zakażenie koronawirusem 2019-nCoV? ',
        'content': 'Nie ma dotychczas żadnych danych wskazujących na to, że zwierzęta domowe, takie jak psy i koty mogą przenosić zakażenie koronawirusem 2019-nCoV. Niemniej zaleca się przestrzeganie zasad higieny, np. mycie rąk wodą z mydłem po kontakcie ze zwierzętami domowymi, ponieważ mogą one przenosić na człowieka różne bakterie (np. E. coli albo Salmonellę).'
    }, {
        'title': 'Czy osoby nieubezpieczone, jeśli zachorują na COVID-19, będą leczone bezpłatnie?',
        'content': 'Tak. Wszyscy pacjenci z objawami koronawirusa będą leczeni bezpłatnie, również osoby nieubezpieczone.'
    }, {
        'title': 'Czy picie alkoholu chroni przed wirusem?',
        'content': 'Nie. Picie alkoholu nie chroni przed wirusem. Alkohol obniża odporność organizmu.'
    }, {
        'title': 'Czy w Polsce zabraknie żywności?',
        'content': 'Nie. W Polsce nie zabraknie żywności. Jesteśmy jednym z największych producentów żywności w Europie.'
    }, {
        'title': 'Czy domowe testy na koronawirusa są skuteczne?',
        'content': 'Nie! Wiarygodny test na obecność koronawirusa może przeprowadzić tylko placówka medyczna.'
    }, {
        'title': 'Czy sklepy zostaną zamknięte? Czy nie będzie można dostać jedzenia i leków?',
        'content': 'Nie. Wszystkie sklepy spożywcze, drogerie i apteki są otwarte – również te w galeriach i centrach handlowych.'
    }, {
        'title': 'Czy jedzenie czosnku może zapobiec infekcji koronawirusem?',
        'content': 'Czosnek to zdrowa żywność, która może mieć pewne właściwości przeciwdrobnoustrojowe. Jednak nie ma obecnie dowodów, że jedzenie czosnku chroni przed nowym koronawirusem.'
    }, {
        'title': 'Czy lampa dezynfekująca na promienie UV może zabić koronawirusa?',
        'content': 'Lampy UV przeznaczone są do dezynfekcji pomieszczeń, nie powinny być używane do sterylizacji rąk lub innych obszarów skóry, ponieważ promieniowanie UV może powodować podrażnienie skóry.'
    }, {
        'title': 'Czy rozpylanie alkoholu lub chloru na całe ciało może zabić koronawirusa?',
        'content': 'Nie. Rozpylanie alkoholu lub chloru na całym ciele nie zabije wirusów, które już dostały się do organizmu. Rozpylanie takich substancji może być szkodliwe dla skóry i błon śluzowych (tj. oczu, ust). Zarówno alkohol, jak i chlor mogą być przydatne do dezynfekcji powierzchni, ale należy je stosować zgodnie z odpowiednimi zaleceniami.'
    }, {
        'title': 'Czy suszarki do rąk skutecznie zabijają koronawirusa?',
        'content': 'Nie. Suszarki do rąk nie są skuteczne w zabijaniu SARS-CoV-2019. Aby uchronić się przed nowym koronawirusem, należy często myć ręce wodą i mydłem oraz dezynfekować je płynem na bazie alkoholu. Po umyciu rąk należy je dokładnie osuszyć, używając ręczników papierowych lub suszarki na gorące powietrze.'
    }, {
        'title': 'Czy regularne płukanie nosa solą fizjologiczną może zapobiec infekcji koronawirusem?',
        'content': 'Nie. Nie ma dowodów na to, że regularne płukanie nosa solą fizjologiczną chroni przed zakażeniem nowym koronawirusem. Istnieją pewne ograniczone dowody, że regularne płukanie nosa solą fizjologiczną może pomóc szybciej zregenerować się śluzówce przy przeziębieniu. Jednak regularne płukanie nosa nie zapobiega infekcjom dróg oddechowych.'
    }, {
        'title': 'Czy szczepionki przeciwko zapaleniu płuc chronią przed koronawirusem?',
        'content': 'Nie. Szczepionki przeciw zapaleniu płuc, takie jak szczepionka przeciwko pneumokokom i szczepionka przeciwko Haemophilus influenza typu B (Hib), nie zapewniają ochrony przed nowym koronawirusem. Wirus jest tak nowy i inny, że jeszcze nie istnieje właściwa szczepionka szczepionki. \nNaukowcy próbują opracować szczepionkę przeciwko SARS-CoV-2019, a WHO wspiera ich wysiłki. '
    }, {
        'title': 'Czy koronawirus przenosi się przez ukąszenia komarów?',
        'content': 'Do tej pory nie było żadnych informacji ani dowodów sugerujących, że nowy koronawirus może być przenoszony przez komary. '
    }, {
        'title': 'Jak skuteczne są skanery termiczne w wykrywaniu osób zainfekowanych koronawirusem?',
        'content': 'Skanery termiczne skutecznie wykrywają osoby, u których występuje gorączka (tj. mających wyższą niż normalna temperatura ciała), w tym gorączka spowodowaną zakażeniem nowym koronawirusem. Skanery nie mogą jednak wykryć osób zarażonych, ale jeszcze niegorączkujących. Dzieje się tak, ponieważ zarażone osoby mogą rozwinąć objaw, jakim jest gorączka, średnio w ciągu od 2 do 10 dni od zarażenia. U części osób zakażenie może przebiegać w ogóle bez wystąpienia gorączki. Osoby te mogą zarażać mimo braku gorączki i nie wykryje ich skaner termiczny.'
    }];
