import {Col, Container, ListGroup, ListGroupItem, Row} from "reactstrap";
import React from "react";
import {NUMBER_SYMPTOMS, SYMPTOM_COLOR_MAPPING, SYMPTOM_MAPPING} from "../consts";

export const CalendarDetails = (props) => {
    const {addNotePageState, setAddNotePageState} = props;

    const items = Object.keys(SYMPTOM_COLOR_MAPPING).reverse();
    const children = items.map((item) => {
        const show = item in addNotePageState.symptoms && addNotePageState.symptoms[item] !== false;
        if (!show){
            return null;
        }
        const isBoolean = !(item in NUMBER_SYMPTOMS);
        const boolVal = item in addNotePageState.symptoms ? addNotePageState.symptoms[item] : false;
        const tickCross = {
            true: '✓',
            false: '✗'
        };
        const displayValue = isBoolean ?
            tickCross[boolVal]
            : addNotePageState.symptoms[item].toString().replace('.', ',');
        const color = SYMPTOM_COLOR_MAPPING[item];
        const classNames = color ? '': 'app-main app-text ';
        const classNamesAll= `${classNames}simple-border`;
        return (
            <ListGroupItem
                className={classNamesAll}
                color={SYMPTOM_COLOR_MAPPING[item]}
            >
                <Row>
                    <Col xs={'10'}>
                        {SYMPTOM_MAPPING[item]}
                    </Col>
                    <Col xs={'1'}>
                        {displayValue}
                    </Col>
                </Row>
            </ListGroupItem>
        )
    });
    const text = (
        <ListGroup>{children}</ListGroup>
    );

    return (
        <Container fluid={true} className={'nopadding'} style={{'overflowY': 'scroll', 'height': '38%'}}>
            <Row className={'nopadding'} style={{width: "100%"}}>
                <Col xs={'12'} className={'nopadding'}>
                    {text}
                </Col>
            </Row>
        </Container>
    )
};