import FullCalendar from "@fullcalendar/react";
import plLocale from '@fullcalendar/core/locales/pl';
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import React from "react";
import {MOMENT} from "../moment";

import '../style.scss';

const calendarComponentRef = React.createRef<FullCalendar>();

export const Calendar = (props) => {
    const {selectedDate, setSelectedDate} = props;

    const handleDateClick = (arg: any) => {
        setSelectedDate(MOMENT(arg.date).format().split('T')[0]);
    };

    const events = [
        {
            title: '✷',
            start: new Date(selectedDate),
        }
    ];

    const eventRender = (info) => {
        const {event, el, isMirror, isStart, isEnd, view} = info;
        const txt = el.text.split(' ')[1]
        el.text = txt;
        el.className = 'event-today';
    };

    return <FullCalendar
        defaultView="dayGridMonth"
        header={{
          center: 'title',
          left: '',
          right: 'prev,next today',
        }}
        locale={plLocale}
        plugins={[dayGridPlugin, interactionPlugin]}
        ref={calendarComponentRef}
        weekends={true}
        events={events}
        dateClick={handleDateClick}
        eventRender={eventRender}
      />;
};