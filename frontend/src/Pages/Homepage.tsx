import React, {useEffect, useState} from 'react';
import {TabPage} from "../Containers/TabPage";
import {MOMENT} from "../moment";
import {POST_GET_DAY_NOTE} from "../routes";

interface HomepageInterface {
  width: number;
}

const handleWindowSizeChange = (setWidth: (value: number | ((prevVar: number) => number)) => void) => {
  setWidth(window.innerWidth);
};

const selectDate = (date: string, setAddNotePageState) => {
    const dateISO = MOMENT(date).toISOString().split('T')[0];
    POST_GET_DAY_NOTE(
        {user_id: 1, date: dateISO},
        (data) => setAddNotePageState({symptoms: data}),
    );
};

export const Homepage = (props: HomepageInterface) => {
    const [width, setWidth] = useState(props.width);
    const [addNotePageState, setAddNotePageState] = useState({symptoms: {}});
    const now = MOMENT().toISOString().split('T')[0];
    const [selectedDate, setSelectedDate] = useState(now);

    useEffect(() =>{
        selectDate(selectedDate, setAddNotePageState);
    }, [selectedDate]);

    useEffect(() => {
        window.addEventListener('resize', () => handleWindowSizeChange(setWidth));
        return () => {
            window.removeEventListener('resize', () => {})
        }
    });

    return (
        <TabPage
            addNotePageState={addNotePageState}
            setAddNotePageState={setAddNotePageState}
            selectedDate={selectedDate}
            setSelectedDate={setSelectedDate}
        />
    )
};