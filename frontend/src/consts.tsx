export const SYMPTOM_KATAR = "katar";
export const SYMPTOM_KATAR_TEXT = "Katar / Zatkany nos";

export const SYMPTOM_KICHANIE = "kichanie";
export const SYMPTOM_KICHANIE_TEXT = "Kichanie";

export const SYMPTOM_NUDNOSCI = "nudnosci";
export const SYMPTOM_NUDNOSCI_TEXT = "Nudności / Wymioty";

export const SYMPTOM_BIEGUNKA = "biegunka";
export const SYMPTOM_BIEGUNKA_TEXT = "Biegunka";

export const SYMPTOM_BOL_BRZUCHA = "bol_brzucha";
export const SYMPTOM_BOL_BRZUCHA_TEXT = "Ból brzucha";

export const SYMPTOM_ZAWROTY_GLOWY = "zawroty_glowy";
export const SYMPTOM_ZAWROTY_GLOWY_TEXT = "Zawroty głowy";

export const SYMPTOM_NIEPOKOJ = "niepokoj";
export const SYMPTOM_NIEPOKOJ_TEXT = "Uczucie niepokoju";

export const SYMPTOM_KOLATANIE_SERCA = "kolatanie_serca";
export const SYMPTOM_KOLATANIE_SERCA_TEXT ="Kołatanie serca";

export const SYMPTOM_ZIME_DRESZCZE = "zime_dreszcze";
export const SYMPTOM_ZIME_DRESZCZE_TEXT = "Uderzenia gorąca / Zimne dreszcze";

export const SYMPTOM_KRWIOPLUCIE = "krwioplucie";
export const SYMPTOM_KRWIOPLUCIE_TEXT ="Krwioplucie";

export const SYMPTOM_SPLATANIE = "splatanie";
export const SYMPTOM_SPLATANIE_TEXT = "Splątanie";

export const SYMPTOM_WYSYPKA = "wysypka";
export const SYMPTOM_WYSYPKA_TEXT ="Wysypka";

export const SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH = "powiekszenie_wezlow_chlonnych";
export const SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH_TEXT ="Powiększenie węzłów chłonnych";

export const SYMPTOM_ZAPARCIA = "zaparcia";
export const SYMPTOM_ZAPARCIA_TEXT ="Zaparcia";

export const SYMPTOM_ZGAGA = "zgaga";
export const SYMPTOM_ZGAGA_TEXT ="Zgaga";

export const SYMPTOM_ZMECZENIE = "zmeczenie";
export const SYMPTOM_ZMECZENIE_TEXT ="Zmęczenie";

export const SYMPTOM_BOL_GLOWY = "bol_glowy";
export const SYMPTOM_BOL_GLOWY_TEXT ="Ból głowy";

export const SYMPTOM_BOL_MIESNI = "bol_miesni";
export const SYMPTOM_BOL_MIESNI_TEXT ="Ból mięśni";

export const SYMPTOM_BOL_GARDLA = "bol_gardla";
export const SYMPTOM_BOL_GARDLA_TEXT ="Ból gardła";

export const SYMPTOM_ZABURZENIE_WECHU = "zaburzenie_wechu";
export const SYMPTOM_ZABURZENIE_WECHU_TEXT ="Zaburzenie węchu";

export const SYMPTOM_ZABURZENIE_SMAKU = "zaburzenie_smaku";
export const SYMPTOM_ZABURZENIE_SMAKU_TEXT ="Zaburzenie smaku";

export const SYMPTOM_KASZEL = "kaszel";
export const SYMPTOM_KASZEL_TEXT ="Kaszel";

export const SYMPTOM_GORACZKA = "goraczka";
export const SYMPTOM_GORACZKA_TEXT ="Gorączka";

export const SYMPTOM_DUSZNOSC = "dusznosc";
export const SYMPTOM_DUSZNOSC_TEXT ="Duszność / Trudności w oddychaniu";

export const SYMPTOM_TEMPERATURA = "temperatura";
export const SYMPTOM_TEMPERATURA_TEXT ="Temperatura";

export const RED = 'danger';
export const YELLOW = 'warning';
export const GREEN = 'success';

export const SYMPTOM_MAPPING = {
    [SYMPTOM_KATAR]                         : SYMPTOM_KATAR_TEXT,
    [SYMPTOM_KICHANIE]                      : SYMPTOM_KICHANIE_TEXT,
    [SYMPTOM_NUDNOSCI]                      : SYMPTOM_NUDNOSCI_TEXT,
    [SYMPTOM_BIEGUNKA]                      : SYMPTOM_BIEGUNKA_TEXT,
    [SYMPTOM_BOL_BRZUCHA]                   : SYMPTOM_BOL_BRZUCHA_TEXT,
    [SYMPTOM_ZAWROTY_GLOWY]                 : SYMPTOM_ZAWROTY_GLOWY_TEXT,
    [SYMPTOM_NIEPOKOJ]                      : SYMPTOM_NIEPOKOJ_TEXT,
    [SYMPTOM_KOLATANIE_SERCA]               : SYMPTOM_KOLATANIE_SERCA_TEXT,
    [SYMPTOM_ZIME_DRESZCZE]                 : SYMPTOM_ZIME_DRESZCZE_TEXT,
    [SYMPTOM_KRWIOPLUCIE]                   : SYMPTOM_KRWIOPLUCIE_TEXT,
    [SYMPTOM_SPLATANIE]                     : SYMPTOM_SPLATANIE_TEXT,
    [SYMPTOM_WYSYPKA]                       : SYMPTOM_WYSYPKA_TEXT,
    [SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH] : SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH_TEXT,
    [SYMPTOM_ZAPARCIA]                      : SYMPTOM_ZAPARCIA_TEXT,
    [SYMPTOM_ZGAGA]                         : SYMPTOM_ZGAGA_TEXT,
    [SYMPTOM_ZMECZENIE]                     : SYMPTOM_ZMECZENIE_TEXT,
    [SYMPTOM_BOL_GLOWY]                     : SYMPTOM_BOL_GLOWY_TEXT,
    [SYMPTOM_BOL_MIESNI]                    : SYMPTOM_BOL_MIESNI_TEXT,
    [SYMPTOM_BOL_GARDLA]                    : SYMPTOM_BOL_GARDLA_TEXT,
    [SYMPTOM_ZABURZENIE_WECHU]              : SYMPTOM_ZABURZENIE_WECHU_TEXT,
    [SYMPTOM_ZABURZENIE_SMAKU]              : SYMPTOM_ZABURZENIE_SMAKU_TEXT,
    [SYMPTOM_KASZEL]                        : SYMPTOM_KASZEL_TEXT,
    [SYMPTOM_GORACZKA]                      : SYMPTOM_GORACZKA_TEXT,
    [SYMPTOM_DUSZNOSC]                      : SYMPTOM_DUSZNOSC_TEXT,
    [SYMPTOM_TEMPERATURA]                   : SYMPTOM_TEMPERATURA_TEXT,
};

export const SYMPTOM_COLOR_MAPPING = {
    [SYMPTOM_KATAR]                         : GREEN,
    [SYMPTOM_KICHANIE]                      : GREEN,
    [SYMPTOM_NUDNOSCI]                      : GREEN,
    [SYMPTOM_BIEGUNKA]                      : GREEN,
    [SYMPTOM_BOL_BRZUCHA]                   : GREEN,
    [SYMPTOM_ZAWROTY_GLOWY]                 : GREEN,
    [SYMPTOM_NIEPOKOJ]                      : GREEN,
    [SYMPTOM_KOLATANIE_SERCA]               : GREEN,
    [SYMPTOM_ZIME_DRESZCZE]                 : GREEN,
    [SYMPTOM_KRWIOPLUCIE]                   : GREEN,
    [SYMPTOM_SPLATANIE]                     : GREEN,
    [SYMPTOM_WYSYPKA]                       : GREEN,
    [SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH] : GREEN,
    [SYMPTOM_ZAPARCIA]                      : GREEN,
    [SYMPTOM_ZGAGA]                         : GREEN,
    [SYMPTOM_ZMECZENIE]                     : YELLOW,
    [SYMPTOM_BOL_GLOWY]                     : YELLOW,
    [SYMPTOM_BOL_MIESNI]                    : YELLOW,
    [SYMPTOM_BOL_GARDLA]                    : YELLOW,
    [SYMPTOM_ZABURZENIE_WECHU]              : YELLOW,
    [SYMPTOM_ZABURZENIE_SMAKU]              : YELLOW,
    [SYMPTOM_KASZEL]                        : RED,
    [SYMPTOM_GORACZKA]                      : RED,
    [SYMPTOM_DUSZNOSC]                      : RED,
    [SYMPTOM_TEMPERATURA]                   : null,
};

export const LOW_RISK_SYMPTOMS = {
    [SYMPTOM_KATAR]                         : SYMPTOM_KATAR_TEXT,
    [SYMPTOM_KICHANIE]                      : SYMPTOM_KICHANIE_TEXT,
    [SYMPTOM_NUDNOSCI]                      : SYMPTOM_NUDNOSCI_TEXT,
    [SYMPTOM_BIEGUNKA]                      : SYMPTOM_BIEGUNKA_TEXT,
    [SYMPTOM_BOL_BRZUCHA]                   : SYMPTOM_BOL_BRZUCHA_TEXT,
    [SYMPTOM_ZAWROTY_GLOWY]                 : SYMPTOM_ZAWROTY_GLOWY_TEXT,
    [SYMPTOM_NIEPOKOJ]                      : SYMPTOM_NIEPOKOJ_TEXT,
    [SYMPTOM_KOLATANIE_SERCA]               : SYMPTOM_KOLATANIE_SERCA_TEXT,
    [SYMPTOM_ZIME_DRESZCZE]                 : SYMPTOM_ZIME_DRESZCZE_TEXT,
    [SYMPTOM_KRWIOPLUCIE]                   : SYMPTOM_KRWIOPLUCIE_TEXT,
    [SYMPTOM_SPLATANIE]                     : SYMPTOM_SPLATANIE_TEXT,
    [SYMPTOM_WYSYPKA]                       : SYMPTOM_WYSYPKA_TEXT,
    [SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH] : SYMPTOM_POWIEKSZENIE_WEZLOW_CHLONNYCH_TEXT,
    [SYMPTOM_ZAPARCIA]                      : SYMPTOM_ZAPARCIA_TEXT,
    [SYMPTOM_ZGAGA]                         : SYMPTOM_ZGAGA_TEXT,
};

export const MID_RISK_SYMPTOMS = {
    [SYMPTOM_ZMECZENIE]           : SYMPTOM_ZMECZENIE_TEXT,
    [SYMPTOM_BOL_GLOWY]           : SYMPTOM_BOL_GLOWY_TEXT,
    [SYMPTOM_BOL_MIESNI]          : SYMPTOM_BOL_MIESNI_TEXT,
    [SYMPTOM_BOL_GARDLA]          : SYMPTOM_BOL_GARDLA_TEXT,
    [SYMPTOM_ZABURZENIE_WECHU]    : SYMPTOM_ZABURZENIE_WECHU_TEXT,
    [SYMPTOM_ZABURZENIE_SMAKU]    : SYMPTOM_ZABURZENIE_SMAKU_TEXT,
};

export const HIGH_RISK_SYMPTOMS = {
    [SYMPTOM_KASZEL]      : SYMPTOM_KASZEL_TEXT,
    [SYMPTOM_GORACZKA]    : SYMPTOM_GORACZKA_TEXT,
    [SYMPTOM_DUSZNOSC]    : SYMPTOM_DUSZNOSC_TEXT,
};

export const NUMBER_SYMPTOMS = {
    [SYMPTOM_TEMPERATURA]: {
        display: SYMPTOM_TEMPERATURA_TEXT,
        placeholder: '36,6'
    }
};