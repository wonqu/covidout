import React, {useState} from "react";
import {Button, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader, TabPane} from "reactstrap";
import {ARTICLES} from "../constsArticles";

import '../style.scss';

const Article = (props) => {
    const {buttonLabel, modalContent} = props;
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    return (
        <div>
          <Button
              style={{width: "100%"}}
              className={'app-text app-background'}
              onClick={toggle}
          >
              {buttonLabel}
          </Button>
          <Modal isOpen={modal} fade={false} toggle={toggle}>
            <ModalHeader
                toggle={toggle}
                className={'app-text app-background'}
            >
                {buttonLabel}
            </ModalHeader>
            <ModalBody
                className={'app-text app-background'}
            >
                {modalContent}
            </ModalBody>
          </Modal>
        </div>
    );
}

export const ArticlePage = () => {
    const articleComponents = ARTICLES.map(({title, content}, index) => (
        <ListGroupItem
            style={{width: "100%"}}
            className={'app-main'}
        >
            <Article
                style={{width: "100%"}}
                buttonLabel={title}
                modalContent={content}
            />
        </ListGroupItem>
    ));
    return (
        <ListGroup style={{width: "100%", height:"90%"}} className={'scroll'}>
            {articleComponents}
        </ListGroup>
    );
};