import {Nav, NavItem, NavLink, TabContent, TabPane} from "reactstrap";
import React, {useState} from "react";
import classnames from 'classnames';

import "./calendar.scss";
import "./tabs.scss";
import "../style.scss";
import {Calendar} from "../Components/Calendar";
import {CalendarDetails} from "../Components/CalendarDetails";
import {AddNotePage} from "./AddNotePage";
import {ArticlePage} from "./ArticlePage";

const CALENDAR_TAB_TEXT = "Kalendarz";
const EDIT_DAY_TEXT = "Edytuj dzień";
const ARTICLES_TEXT = "Artykuły";


const Tab = (tabNum: string, text: string, activeTab, setActiveTab) => {
  const toggle = (tab: string) => {
    if(activeTab !== tab) setActiveTab(tab);
  };

  return (
      <NavItem className={classnames({ active: activeTab === tabNum })}>
        <NavLink
          onClick={() => { toggle(tabNum); }}
        >
          {text}
        </NavLink>
      </NavItem>
  );
};


export const TabPage = (props) => {
  const [activeTab, setActiveTab] = useState('1');
  const {addNotePageState, setAddNotePageState} = props;
  const {selectedDate, setSelectedDate} = props;

  const calendarTab = Tab('1', CALENDAR_TAB_TEXT, activeTab, setActiveTab);
  const otherTab = Tab('2', EDIT_DAY_TEXT, activeTab, setActiveTab);
  const articleTab = Tab('3', ARTICLES_TEXT, activeTab, setActiveTab);

  const calendar = <Calendar
      selectedDate={selectedDate}
      setSelectedDate={setSelectedDate}
  />;
  const calendarDetails = <CalendarDetails
      addNotePageState={addNotePageState}
      setAddNotePageState={setAddNotePageState}
  />;

  const addNote = <AddNotePage
      date={selectedDate}
      addNotePageState={addNotePageState}
      setAddNotePageState={setAddNotePageState}
  />;

  return (
    <div className={"page"}>
      <Nav tabs className={'app-main app-text'}>
        {calendarTab}
        {otherTab}
        {articleTab}
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          {calendar}
          {calendarDetails}
        </TabPane>
        <TabPane tabId="2">
          {addNote}
        </TabPane>
        <TabPane tabId="3" className={'scroll'}>
          <ArticlePage/>
        </TabPane>
      </TabContent>
    </div>
  );
};