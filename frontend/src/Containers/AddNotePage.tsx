import {
    Button,
    Col, Container,
    FormGroup,
    Input,
    Label,
    ListGroup,
    ListGroupItem,
    Row
} from "reactstrap";
import React, {useState} from "react";
import {
    HIGH_RISK_SYMPTOMS,
    LOW_RISK_SYMPTOMS,
    MID_RISK_SYMPTOMS,
    NUMBER_SYMPTOMS,
} from "../consts";
import {POST_UPLOAD_SUMPTOMS} from "../routes";
import {MOMENT} from "../moment";

const SAVE_TEXT = "Zapisz";

const booleanSymptomWidget = (key, display, value, color, state, setState) => {
    const handleChange = (event) => {
        const newValue = event.target.checked;
        console.log(newValue);
        const newState = {
            symptoms: {
                ...state.symptoms,
                [key]: newValue
            }
        };
        setState(newState);
    };

    return(
        <ListGroupItem color={color}>
            <FormGroup check>
                <Label for={key} style={{width: "100%"}} check>
                  <Input id={key} type="checkbox" value={value} onChange={handleChange}/>{' '}
                    <span style={{width: "100%"}}>{display}</span>
                </Label>
              </FormGroup>
        </ListGroupItem>
    )
};

const fixNumber = (txt: string) => {
    return txt.replace(',', '.');
};

const fixState = (key, value, state, setState) => {
  const newState = {
      symptoms: {
          ...state.symptoms,
          [key]: parseFloat(fixNumber(value.toString()))
      }
  };
  setState(newState);
};

const numberSymptomWidget = (key, display, value, placeholder, state, setState) => {
    const newValue = key in state.symptoms ? state.symptoms[key] : '';

    const handleChange = (event) => {
        const newValue = event.target.value;
        const newState = {
            symptoms: {
                ...state.symptoms,
                [key]: newValue
            }
        };
        setState(newState);
    };

    return (
        <ListGroupItem className={'app-main app-text'}>
            <FormGroup>
                <Label for={key}>{display}</Label>
                <Input
                    type="number"
                    id={key}
                    placeholder={placeholder}
                    value={value}
                    onChange={handleChange}
                    onBlur={(event) => fixState(key, newValue, state, setState)}/>
            </FormGroup>
        </ListGroupItem>
    )
};

const formatDate = (date: string) => {
  return MOMENT(date).format('DD.MM.YYYY')
};

export const AddNotePage = (props) => {
  const {date} = props;
  const {addNotePageState, setAddNotePageState} = props;
  const sendUpdateRequest = () => {
      POST_UPLOAD_SUMPTOMS(
          {
              ...addNotePageState,
              user_id: 1,
              date: MOMENT(date).toISOString().split("T")[0],
          },
          () => {}
      );
  };

  const defaultValue = (key, default_) => key in addNotePageState.symptoms ? addNotePageState.symptoms[key] : default_;

  const listItemsNumber = Object.entries(NUMBER_SYMPTOMS).map(
      ([key, params]) => {
          return numberSymptomWidget(
              key,
              params.display,
              defaultValue(key, ''),
              params.placeholder,
              addNotePageState,
              setAddNotePageState,
          )
      }
  );

  const listItemsHighRisk = Object.entries(HIGH_RISK_SYMPTOMS).map(
      ([key, display]) => {
          return booleanSymptomWidget(
              key,
              display,
              defaultValue(key, false),
              'danger',
              addNotePageState,
              setAddNotePageState
          )
      }
  );

  const listItemsMedRisk = Object.entries(MID_RISK_SYMPTOMS).map(
      ([key, display]) => {
          return booleanSymptomWidget(
              key,
              display,
              defaultValue(key, false),
              'warning',
              addNotePageState,
              setAddNotePageState
          )
      }
  );

  const listItemsLowRisk = Object.entries(LOW_RISK_SYMPTOMS).map(
      ([key, display]) => {
          return booleanSymptomWidget(
              key,
              display,
              defaultValue(key, false),
              'success',
              addNotePageState,
              setAddNotePageState
          )
      }
  );
  return (
    <Container className={'nopadding'} style={{height: "100%"}}>
        <Row className={'app-main app-text'}>
            <Col xs={'6'} style={{'textAlign': 'center'}}>
                <h3>{formatDate(date)}</h3>
            </Col>
            <Col xs={'6'} style={{'textAlign': 'right'}}>
                <Button
                    onClick={(event) => sendUpdateRequest()}
                    className={"app-background app-text"}
                >
                    {SAVE_TEXT}
                </Button>
            </Col>
        </Row>
        <Row style={{height: "100%"}}>
            <Col xs={'12'} className={'scroll nopadding'} style={{height: "100%"}}>
                <ListGroup>
                    {listItemsNumber}
                    {listItemsHighRisk}
                    {listItemsMedRisk}
                    {listItemsLowRisk}
                    {
                        /*
                        this is not very pretty but we're adding
                        4 list items so that last lowRisk item won't be hidden.
                        */
                    }
                    <ListGroupItem/>
                    <ListGroupItem/>
                    <ListGroupItem/>
                    <ListGroupItem/>
                </ListGroup>
            </Col>
        </Row>
        </Container>
  )
};