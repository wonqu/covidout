from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import datetime

@contextmanager
def db_connection():
    """ Connector to database.
    """
    conn_string = 'postgresql+psycopg2://admin:admin@95.179.154.232:5432/cvdb'
    engine = create_engine(conn_string)
    session = scoped_session(sessionmaker(bind=engine))
    try:
        yield session()
    finally:
        session.remove()


def set_with_query(query: str):
    """ Execute_query
    :param query: insert/delete query to evaluate
    Example of query:
        query = "DELETE FROM [Vcd.CounterValues] WHERE Timestamp < DATEADD(DAY, {-num_of_days}, GETDATE())"
    """
    with db_connection() as handle:
        try:
            handle.execute(query)
        except Exception as e:
            print(e)
        handle.commit()


def get_with_query(query: str):
    """ Execute_query
    :param query: selection query to evaluate
    Example of query:
        query = "SELECT FROM [Vcd.CounterValues] WHERE Timestamp < DATEADD(DAY, {-num_of_days}, GETDATE())"
    """
    with db_connection() as handle:
        result = handle.execute(query)
        row = result.fetchone()
        if not row:
            return {}

        dict = {}
        for k in row.keys():    
            dict[k] = row[k]          

        result.close()
        return dict


def insert_sql_from_json_str(table_name: str, user_id: int, date:dict, json: dict) -> str:
    """
    :param table_name: name of table
    :param user_id:
    :param date:
    :param json: json in dict form
    :return insert statement
    """
    date = datetime.datetime.strptime(date['date'], '%Y-%m-%d')
    insert_str = f"INSERT INTO {table_name} (user_id, date, {', '.join(json.keys())}) \n" \
                 f"Values ({user_id}, to_timestamp('{date.date()}', 'yyyy-mm-dd'), {', '.join(map(str, json.values()))})"
    return insert_str


def insert_sql_from_json_str_with_timestamp(table_name: str, user_id: int, date: dict, json: dict) -> str:
    """
    :param table_name: name of table
    :param user_id:
    :param date:
    :param json: json in dict form
    :return insert statement
    """
    print(date)
    insert_str = f"INSERT INTO {table_name} (user_id, datatime, {', '.join(json.keys())}) \n" \
                 f"Values ({user_id}, to_timestamp('{date}', 'yyyy-mm-dd HH24:MI:SS'), {', '.join(map(str, json.values()))})"
    return insert_str

def select_symptoms(user_id: int, date: dict) -> str:
    """
    :param date:
    :param user_id:
    :return insert statement
    Example:
    SELECT * FROM table_name WHERE Timestamp = ...
    """

    date = datetime.datetime.strptime(date['date'], '%Y-%m-%d')

    select_str = f"SELECT * FROM Symptoms WHERE date = '{date.date()}' AND user_id = {user_id}"
    return select_str

