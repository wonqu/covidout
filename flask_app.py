import os

from flask import Flask, jsonify, request, send_from_directory
import connection
import json
import random
app = Flask(__name__, static_folder='frontend/build')


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def home(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')


@app.route('/calendar', methods=['GET','POST'])
def get_day_note():
    """
    Get symptoms for given day
    URL example: http://127.0.0.1:5000/calendar/1/2020-03-21
    :return: symptoms in json - keywords form readme.md
    :param user_id: specified user
    :param date: date in format {'date':'yyyy-mm-dd'}
    :return: symptoms which are not none - per day, 
             empty dict if there isn't note for chosen day
    """
    data = json.loads(request.data)
    user_id = data["user_id"]
    date = data["date"]
    date_dict = {'date': date}
    query = connection.select_symptoms(user_id, date=date_dict)
    result = connection.get_with_query(query)
    if result:
        result.pop('date')
        result.pop('user_id')

        result = {k: v for k, v in result.items() if v is not None} # remove all null values
    # result = {'kaszel':True, 'temperatura': 37.1, 'duszność':True}
    return jsonify(result)

@app.route('/notfictions')
def get_notfictions():
    ''' 
    Function returns notfiction for current day, e.g. stay home
    URL example: http://127.0.0.1:5000/notfictions/
    :returns: json with strings
    '''
    rand = random.randrange(100)
    list_of_not = ['Robisz kawał dobrej roboty zostając w domu!', 'Razem przetrwamy tego wirusa!',
                   'Pomóż osobom starszym!', 'Uszy do góry!']
    if rand < 4:
        result = list_of_not[rand]
        result = {'notfictions': [result]}
    else:
        result = {'notfictions': []}
    return jsonify(result)


@app.route('/contact/<user_id>', methods=['GET','POST'])
def get_contact_numbers(user_id):
    ''' uses history of localisation
    URL example: http://127.0.0.1:5000/contact/1
    :return: contact oto to nearliest pandemic point
    '''
    result = {'location_name':'Szpital kliniczny we Wroclawiu', 'number': '+48 123 456 789'}
    return jsonify(result)


@app.route('/sumptoms', methods=['GET','POST'])
def upload_symptoms():
    '''
    Symptoms format - json with keywords form readme.md
    data format:
        {
        user_id: number
        data : date
        symptoms: {
            temperature: real
            kaszel: boolean
            niepokoj: boolean
        }
    '''
    # parsing data
    data = json.loads(request.data)
    user_id = data["user_id"]
    date = {'date': data["date"]}
    symptoms = data["symptoms"] 

    table_name = 'symptoms'
    # Create query from json
    query = connection.insert_sql_from_json_str(table_name, user_id, date, symptoms)
    # Evaluate query
    connection.set_with_query(query)
    return "ok"


@app.route('/location', methods=['GET', 'POST'])
def upload_gps():
    '''
    Example URL http://127.0.0.1:5000/location'
    {
    "user_id": 1,
    "gps": {
      "datetime": "2020-03-21  15:00:30",
      "long": 50,
      "lang": 30
    }
    }
    '''
    data = json.loads(request.data)
    user_id = data["user_id"]
    date = data["gps"]["datetime"]
    longitude = data["gps"]["long"]
    latitude = data["gps"]["lang"]
    print(data)
    table_name = 'Locations'
    query = connection.insert_sql_from_json_str_with_timestamp(table_name, user_id, date,
                                                               {'long': longitude, 'lang': latitude})
    connection.set_with_query(query)

    return "ok"


@app.route('/emptyday/<user_id>/<date>')
def is_day_empty(user_id, date):
    ''' Function uses to check if user upload symptoms for data
    date format {'date':'yyyy-mm-dd'}
    Trello: podsumowanie dnia
    :returns: json: {'uploaded':True}
    '''
    result = {'uploaded':True}
    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
