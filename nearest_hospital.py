import pandas as pd
pd.set_option('display.max_columns', None)


def nearest_hospital(coords):
    df = pd.read_csv('CoronavirusPL - Isolation_wards.csv', index_col=False)
    distances = []
    for i, row in df.iterrows():
        dist = pow(row['B'] - coords[0], 2) + pow(row['L'] - coords[1], 2)
        distances.append(dist)

    df['dist'] = distances
    df = df.sort_values(by=['dist'])
    return df.iloc[0][0]
